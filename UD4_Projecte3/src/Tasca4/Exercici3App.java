package Tasca4;

public class Exercici3App {
	
	public static void main(String[] args) {
		/* Declarem dues variables X i Y de tipus int, dues
		 * N i M de tipus double i assignem valors. Mostrem per 
		 * pantalla diferents operacions.
		 */
		int X = 20, Y = 5, suma = X + Y, dif = X - Y, prod = X*Y, div = X/Y, rest = X%Y;
		double N =4.5, M = 2.5, suma2 = N + M, dif2 =  N -  M, prod2 =  N*M, div2 = N/M,rest2 = N%M;
		double suma3 = X + N, div3 = Y/M, rest3 = Y%M;
		System.out.println("Variable X,Y,N,M respectivament = "+ X + ", " + Y +", "+ N + ", " + M);
		System.out.println("Suma X + Y = "+ suma);
		System.out.println("Diferencia X - Y = "+ dif);
		System.out.println("Producte X * Y = "+ prod);
		System.out.println("Div X / Y = "+ div);
		System.out.println("Residu X % Y = "+ rest);
		System.out.println("Suma N + M = "+ suma2);
		System.out.println("Diferencia N - M = "+ dif2);
		System.out.println("Producte N * M = "+ prod2);
		System.out.println("Div N / M = "+ div2);
		System.out.println("Residu N % M = "+ rest2);
		System.out.println("Suma X + N = "+ suma3);
		System.out.println("Divisi� Y / M = "+ div3);
		System.out.println("Residu Y % M = "+ rest3);
		System.out.println("Doble de cada variable X,Y,N,M respectivament = "+ 2*X + ", " + 2*Y +", "+ 2*N + ", " + 2*M);
		System.out.println("Suma de totes les variables = "+ (X+Y+M+N));
		System.out.println("Producte de totes les variables = "+ (X*Y*M*N) );
		
	}
}